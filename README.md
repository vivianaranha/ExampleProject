# Enterprise Mobility Components for Xamarin
Enterprise Mobility Components for Xamarin are a collection of components created by Enterprise Mobility Group.


### Nuger Package Source
In the package manager settings, add the following URL to the list of Package Sources:

[http://10.6.170.72/EMG](http://10.6.170.72/EMG)



### Components
Here is a list of all the components.



|Name|Description|ReadMe|
| ------------------- | --------------------------------- | :-----------: |
|Airport Weather |Provides weather information for an airport|[Coming Soon](http://www.comingsoon.com)|
|Camera |Component to use device camera for photo or video|[Coming Soon](http://www.comingsoon.com)|
|Connectivity |Connectivity Solution|[Coming Soon](http://www.comingsoon.com)|


